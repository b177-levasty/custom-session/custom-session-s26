// #1

db.fruits.aggregate([
    { $unwind: "$supplier_id" },
    { $group: { _id: "$supplier_id", Average: { $avg: "$stock" } } },
]);