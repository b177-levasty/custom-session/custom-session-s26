
db.fruits.aggregate([
    { $match: { onSale: true} },
    { $project: { origin: "Philippines", Max: { $max: { $multiply: [ "$price", "$stock" ] } } },
]);

