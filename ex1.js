let http = require("http");

const port = 4001;

http.createServer(function (request, response) {
	if(request.url == '/welcome'){
		response.writeHead(200, {'content-type': 'text/plain'})
		response.end('Welcome to the world of Node.js');
	}
		else if (request.url == '/register'){
		response.writeHead(503, {'content-type': 'text/plain'})
		response.end('Page is under maintenance');
	}
		else{
		response.writeHead(404, {'content-type': 'text/plain'})
		response.end('Page not found');
	}
}).listen(4001);

console.log('Server running at localhost:4000');